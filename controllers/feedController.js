/**
 * Created by andre on 7/05/17.
 */
'use strict';
var feedParser = require('../services/feedParser');
var transformer = require('../services/transformerData');
var models = require('../models');

module.exports.list = function (req, res, url, next) {

    var statusCode = 200;
    if (req.query.title || req.query.date) {
        getSpecificArticles(req, res);
    }
    feedParser.getItems(url, next)
        .then(function (articles) {
            saveArticles(articles, req.params.id, next);
            transformer.send(res, articles, statusCode, null);
        });
};

function getSpecificArticles(req, res) {
    var query = createQuery(req);
    console.log('HERE');
    console.log(query);
    models.Feed.findAll({
        where: query
    }).then(function (result) {
        res.json(result);
    });
}

function saveArticles(articles, id, next) {
    articles.forEach(function (item) {
        models.Feed.findOrCreate({
            where: {
                link: item.link
            },
            defaults: {
                title: item.title,
                link: item.link,
                date: item.date,
                WebPageId: id
            }
        }).then(
            function (data) {

            },
            function (err) {
                err.message = 'Something Wrong';
                err.status = 500;
                next(err);
            }
        );
    });
}


function createQuery(req) {
    var parameters = req.query;
    var query = {};
    query.WebPageId = req.params.id;
    if (parameters.date) {
        query.date = parameters.date;
    }

    if (parameters.title) {
        query.title = {
            $like: '%' + parameters.title + '%'
        }
    }

    return query;
}