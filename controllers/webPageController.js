/**
 * Created by andre on 7/05/17.
 */
var models = require('../models');
var feedController = require('./feedController');
var transformer = require('../services/transformerData');

module.exports.save = function (req, res, next) {
    models.WebPage.findOrCreate({
        where: {
            domain: req.body.domain
        }
    }).then(
        function (result) {
            var webPage = result[0],
                created = result[1];
            var data = {message: null};
            if (!created) {
                data.message = 'already exists';
            } else {
                data.message = 'URL Added';
            }
            transformer.send(res, data, 200, null);
        },
        function (err) {
            next(err);
        }
    );
};
module.exports.read = function (req, res, next) {
    models.WebPage.findOne({
        where: {
            id: req.params.id
        }
    }).then(
        function (webPage) {
            if (webPage) {
                feedController.list(req, res, webPage.domain, next);
            } else {
                var err = new Error('URL doesn\'t exist');
                err.status = 500;
                next(err);
            }
        },
        function (err) {
            next(err);
        }
    );
};

module.exports.list = function (req, res, next) {
    models.WebPage.findAll({}).then(
        function (dataList) {
            if (dataList) {
                var statusCode = 200;
                transformer.send(res, dataList, statusCode, null);
            }
        },
        function (err) {
            next(err);
        }
    );
};