var express = require('express');
var feedParser = require('../middlewares/feed-middleware');
var router = express.Router();
var webPageController = require('../controllers/webPageController');
var feedController = require('../controllers/feedController');

router.post('/url', feedParser.isValidUrl, function (req, res, next) {
    webPageController.save(req, res, next);
});

router.get('/url', function (req, res, next) {
    webPageController.list(req, res, next);
});

router.get('/url/:id', function (req, res, next) {
    webPageController.read(req, res, next);
});

module.exports = router;
