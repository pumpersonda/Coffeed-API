const feedparser = require('feedparser-promised');
const url = 'http://isValidUrl.bbci.co.uk/news/rss.xml';
const transformer = require('../services/transformerData');

exports.isValidUrl = function (req, res, next) {
    feedparser.parse(req.body.domain)
        .then(
            function () {
                next();
            },
            function (err) {
                next(err);
            }
        );
};

