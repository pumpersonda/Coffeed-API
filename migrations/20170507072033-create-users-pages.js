'use strict';
module.exports = {
    up: function (queryInterface, Sequelize) {
        return queryInterface.createTable('UsersFeeds', {
            username: {
                allowNull: false,
                primaryKey: true,
                type: Sequelize.STRING,
                references:{
                    model: 'Users',
                    key: 'username'
                }
            },
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
                references:{
                    model: 'WebPages',
                    key: 'id'
                }
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: function (queryInterface, Sequelize) {
        return queryInterface.dropTable('user.isValidUrl');
    }
};