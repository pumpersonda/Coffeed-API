'use strict';
module.exports = function (sequelize, DataTypes) {
    var WebPage = sequelize.define('WebPage', {
        domain: DataTypes.STRING,
        name: DataTypes.STRING,
        description: DataTypes.STRING
    }, {
        classMethods: {
            associate: function (models) {
                WebPage.hasMany(models.Feed, {
                    onDelete: "CASCADE"
                });
            }
        }
    });
    return WebPage;
};