'use strict';
module.exports = function (sequelize, DataTypes) {
    var Feed = sequelize.define('Feed', {
        link: {
            type: DataTypes.STRING,
            primaryKey: true
        },
        title: DataTypes.STRING,
        date: DataTypes.DATE,
        WebPageId: DataTypes.INTEGER
    }, {
        classMethods: {
            associate: function (models) {
                Feed.belongsTo(models.WebPage, {});
            }
        }
    });
    return Feed;
};