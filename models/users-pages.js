/**
 * Created by andre on 7/05/17.
 */
'use strict';
module.exports = function (sequelize, DataTypes) {
    var UsersPages = sequelize.define('UserPages', {
        username: {
            type: DataTypes.STRING,
            primaryKey: true
        },
        domain: {
            type: DataTypes.STRING,
            primaryKey: true
        }
    }, {
        classMethods: {
            associate: function (models) {

            }
        }
    });
    return UsersPages;
};