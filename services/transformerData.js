/**
 * Created by andre on 7/05/17.
 */


module.exports.send = function (res,data,statusCode,metaData) {
    res.status(statusCode);
    res.json({
        data:data,
        meta:metaData
    });
};