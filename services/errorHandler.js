/**
 * Created by andre on 7/05/17.
 */
'use strict';
var transformer = require('../services/transformerData');

function errorHandler(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    var statusCode = err.status || 500;
    transformer.send(res,err.message,statusCode,err.statusCode);
}

function errorNotFound(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
}

module.exports.main = errorHandler;
module.exports.errorNotFound = errorNotFound;