/**
 * Created by andre on 7/05/17.
 */
'use strict';
const feedparser = require('feedparser-promised');
const url = 'http://isValidUrl.bbci.co.uk/news/rss.xml';
const transformer = require('../services/transformerData');
const Feed = require('./Feed');


module.exports.getItems = function (url, next) {
    var items = feedparser.parse(url)
        .then(
            function (articles) {
                var data = [];
                articles.forEach(function (item) {

                    var feed = new Feed(
                        item.title,
                        item.description,
                        item.link,
                        item.date,
                        item.origlink
                    );
                    data.push(feed);
                });
                return data;
            },
            function (err) {
                next(err);
            }
        );
    return items;

};