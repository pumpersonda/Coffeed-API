/**
 * Created by andre on 7/05/17.
 */
'use strict';

function Feed(title, description, link, date, domain) {
    this.title = title;
    this.description = description;
    this.link = link;
    this.date = date;
    this.domain = domain;
}

module.exports = Feed;