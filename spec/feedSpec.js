var request = require('request'),
    should = require('should'),
    path = require('path');
var base_url = "http://localhost:3000/";

describe("Extension Server", function() {
    describe("GET /", function() {
        it("returns status code 404", function(done) {
            request.get(base_url, function(error, response, body) {
                expect(response.statusCode).toBe(404);
                done();
            });
        });
    });
});